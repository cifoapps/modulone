import 'package:flutter/material.dart';

class PersonScreen extends StatelessWidget {
  const PersonScreen({
    super.key,
    required this.name,
  });

  final String name;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.redAccent,
          title:
               Text(name),
        ),
      body: Center(
        child: ElevatedButton(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(Color.fromARGB(255, 255, 236, 243)),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
          child: const Center(
            child: Text(
              "👈 Go Back 👈",
              style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold, color: Colors.black87),
            ),
          ),
        ),
      ),
    );
  }
}
