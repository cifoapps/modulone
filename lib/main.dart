import 'package:flutter/material.dart';
import 'package:modulu/people_list_widget.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Generador de noms de persona ficticies',
      theme: ThemeData.light(),
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.redAccent,
          title:
              const Center(child: Text("Generador de noms personals ficticis")),
        ),
        body: const Center(
          child: PeopleList(),
        ),
      ),
    );
  }
}
