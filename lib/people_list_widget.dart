import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:modulu/person_screen.dart';
import 'package:random_name_generator/random_name_generator.dart';

class PeopleList extends StatefulWidget {
  const PeopleList({super.key});

  @override
  State<PeopleList> createState() => _PeopleListState();
}

class _PeopleListState extends State<PeopleList> {
  final _suggestions = <String>[];
  final _style = GoogleFonts.playfair(
      textStyle: const TextStyle(
          fontSize: 22, color: Colors.black87, letterSpacing: .5));

  final _randomNames = RandomNames(Zone.us);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      padding: const EdgeInsets.all(16.0),
      itemBuilder: (context, i) {
        (i >= _suggestions.length)
            ? _suggestions.addAll(
                List.generate(
                  10,
                  (_) => _randomNames.fullName(),
                ),
              )
            : null;
        return Dismissible(
          key: UniqueKey(),
          onDismissed: (_) {
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(
                content: Center(
                  child:
                      Text("It's sad that you suppressed ${_suggestions[i]}"),
                ),
              ),
            );
            setState(() {
                _suggestions.removeAt(i);
              },
            );
          },
          child: GestureDetector(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => PersonScreen(name: _suggestions[i]),
                ),
              );
            },
            child: Card(
              elevation: 0,
              shape: const RoundedRectangleBorder(
                side: BorderSide(
                  color: Colors.black87,
                ),
                borderRadius: BorderRadius.all(
                  Radius.circular(12),
                ),
              ),
              child: SizedBox(
                width: double.infinity,
                height: 60,
                child: Center(
                  child: Text(
                    _suggestions[i],
                    style: _style,
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
