# Modul One

## Descripción

Este proyecto es una aplicación de Flutter desarrollada como parte de un curso de Flutter en el CIFO de L'Hospitalet.

## Pantallas

[![modul1screen2.png](https://i.postimg.cc/L6jv7nQT/modul1screen2.png)](https://postimg.cc/TpYr5Y6L)

## Requisitos Previos

Antes de comenzar, asegúrate de tener instalado lo siguiente:

- [Flutter](https://docs.flutter.dev/get-started/install)
- [Dart](https://dart.dev/get-dart)

## Instalación

1. Clona este repositorio:
```bash
https://gitlab.com/cifoapps/modulone.git
```
2. Navega al directorio del proyecto:
```bash
cd modulone
```
3. Actualiza las dependencias:
```bash
flutter pub get
```
